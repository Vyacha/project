package project

import (
	//"strings"
	"fmt"
	"strings"

	"github.com/essentialkaos/translit/v2"
	"gitlab.com/stud777/stuff/person"
)

type Person struct {
	name    string
	surname string
	midname string
}

const (
	nullValue = "NULL"
)

func main() {
	mtd := person.Metodist{}
	str, err := mtd.GetUserDataString()
	if err != nil {
		panic(err)
	}

	var usrStrChan chan string = make(chan string)               // создание каналов для передачи
	var usrDataChan person.UserData = make(chan person.UserData) // создание каналов для передачи
	defer close(usrDataChan)                                     // закрытие каналов
	defer close(usrStrChan)                                      // второй вариант

	for i := 0; i < 50; i++ {
		go usrStringProducer(usrStrChan) // 50 строк будут распиханы по потокам - рутинам
	}

	for i := 0; i < 50; i++ {
		go usrStringProcessor(usrStrChan, usrDataChan)
	}

	usrDatas := []person.UserData() // массив
	for {
		usrData, ok := <-usrDataChan // ok - нормально прошло чтение из канала или нет
		if !ok {
			break
		}

		fmt.Printf("%d - %v+\n", len(usrDatas), usrData)

		usrDatas = append(usrDatas, usrData)
		if len(usrDatas) == 50 {
			//close(usrDataChan) // закрытие каналов
			//close(usrStrChan)
			break
		}
		//break
	}

	rawUsdData := oper.GetUserdataFromString(str)
	//usdData := getUserdataFromString(str)
	fmt.Printf("%+v\n", str)
	emptyRow := oper.MoodleUserRow{}
	oper.WriteMoodleUserRows()
	//print(getMoodleUsername(usdData))
	//fmt.PrintLn(UserData.Surname)
	// ...

}

//кодинг многопоточности
func usrStringProducer(usrStrChan chan<- string) {
	mtd := person.Metodist{}
	str, _ := mtd.GetUserDataString()
	usrStrChan <- str
}
func usrStringProcessor(usrStrChan <-chan string, usrDataChan chan<- person.UserData) {
	str := <-usrStrChan
	rawUsdData := oper.GetUserdataFromString(str)
	usrDataChan <- rawUsdData
}

//конец многопоточности

func getMoodleUsername(uD person.UserData) string {
	moodleUsername := translit.EncodeToICAO(strings.ToLower(uD.Surname))

	if len(strings.TrimSpace(uD.Name)) > 0 {
		firstLetter := translit.EncodeToICAO(strings.ToLower(string(uD.Name)))
		moodleUsername += firstLetter
	}
	if len(strings.TrimSpace(uD.Midname)) > 0 {
		firstLetter := translit.EncodeToICAO(strings.ToLower(string(uD.Midname)))
		moodleUsername += firstLetter
	}
	return moodleUsername
}

func getUserdataFromString(str string) person.UserData {
	usdData := person.UserData{}
	strAsSlice := strings.Split(str, " ")
	//println(strAsSlice[0])

	usdData.Surname = strAsSlice[0]
	usdData.Name = strAsSlice[1]
	usdData.Midname = strAsSlice[2]
	if strAsSlice[3] != nullValue {
		usdData.SetEmail(strAsSlice[3])
	}
	if strAsSlice[4] != nullValue {
		usdData.SetPasswd(strAsSlice[4])
	}

	if len(strAsSlice) > 5 && len(strings.TrimSpace(strAsSlice[5])) > 0 {
		usdData.IsProfessor = true
	}
	return usdData
}
