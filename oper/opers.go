func getUserdataFromString(str string) person.UserData {
	usdData := person.UserData{}
	strAsSlice := strings.Split(str, " ")
	//println(strAsSlice[0])

	usdData.Surname = strAsSlice[0]
	usdData.Name = strAsSlice[1]
	usdData.Midname = strAsSlice[2]
	if strAsSlice[3] != nullValue {
		usdData.SetEmail(strAsSlice[3])
	}
	if strAsSlice[4] != nullValue {
		usdData.SetPasswd(strAsSlice[4])
	}

	if len(strAsSlice) > 5 && len(strings.TrimSpace(strAsSlice[5])) > 0 {
		usdData.IsProfessor = true
	}
	return usdData
}